from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class Schedule(models.Model):

	# day_choices = (
	# 	(0, 'Sunday'), 
	# 	(1, 'Monday'),
	# 	(2, 'Tuesday'),
	# 	(3, 'Wednesday'), 
	# 	(4, 'Thursday'),
	# 	(5, 'Friday'),
	# )

	day = models.CharField(max_length = 10)
	date = models.DateTimeField(default = datetime.now)
	time = models.TimeField()
	activity = models.CharField('activity', max_length = 30)
	place = models.CharField('place', max_length = 30)
	category = models.CharField('category', max_length = 30)
	
		 