from django.shortcuts import render
from django.shortcuts import redirect
from datetime import datetime, date
from .models import Schedule
from . import forms
from django.http import HttpResponse

def home(request):
    return render(request, 'homepage.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def portofolio(request):
    return render(request, 'portofolio.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
	schedules = Schedule.objects.all().values().order_by('date')
	return render(request, 'schedule.html', {'schedules': schedules})

def create_schedule(request):
	if request.method == 'POST':
		form = forms.ScheduleForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('schedule')
	else:
		form = forms.ScheduleForm()
	
	return render(request, 'create_schedule.html', {'form': form})

def schedule_clear(request, id):
	if request.method == 'POST':
	    Schedule.objects.filter(id=id).delete()
	    return redirect('schedule')
	else:
	    return HttpResponse("/GET not allowed")




